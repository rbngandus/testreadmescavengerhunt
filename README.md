# TestReadMeScavengerHunt

![How it works](images/logo.png)

Scavenger hunt is a web platform for a QR reading based quiz game.
Through any mobile device camera it can scan different QR codes that start different quizes.

- [Introduction](#introduction)
- [Config](#config)
- [Project additional infos](#infos)


## <a name="introduction"></a>Introduction
For any scanned QR the web platform serves a set of question and possible answers (behaving like radio buttons). 
If the answer given by user is correct the platform serves a badge with a hint of the position of next QR to scan.

### How it works
1. User is presented with a first QR
2. User scans QR
3. User gets redirected to web app
4. User is presented a question and answers
5. User answers correctly
6. User is given a victory collectible item
7. User moves to the next QR code and repeat

![How it works](images/flow.png)


## <a name="config"></a>Config
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 



## <a name="infos"></a>Project additional infos

### Project purpose
Scavenger hunt has been developed as a game to explore archeological and natural areas and to learn interesting facts about them.
These sites have been previously equipped with multiples QR reading stations, placed on the areas hotspots.

![Example of the customized app for Villa dei Quintili](images/example.png)

Examples of areas where Scanvenger hunt has been used are:
- Villa dei quintili – Roman villa archeological site
- Explora botanical garden
- Centrale del latte di Roma – dairy production facility

### Related Explora's project

///




